import Network.HTTP
import Text.HTML.TagSoup
import Control.Monad
import qualified Data.Map.Strict as Map
import Data.List
import Data.Ord
import Data.Tuple
import Data.Char

bodiesFromURLs = fmap concat . sequence . map ((=<<) getResponseBody . simpleHTTP . getRequest)

harvest :: [Tag String] -> [String]
harvest = snd . foldl handleTag (False, []) where
    handleTag (False, strings) (TagOpen "p" _) = (True,strings)
    handleTag (True, strings) (TagText a) = (True, a : strings)
    handleTag (True, strings) (TagClose "p") = (False, strings)
    handleTag state _ = state

corpus :: [String] -> [String]
corpus = filter (\x -> isAlpha (x !! 0)) . filter (\x -> length x > 0) . map (filter (\x -> isAscii x && ((isAlpha x)) || (isSpace x) || x `elem` "-.'!;")) . words . concat

type PrimitiveModel = Map.Map (String,String) [String]

generatePrimitiveModel :: String -> String -> [String] -> PrimitiveModel
generatePrimitiveModel a b corpus = case corpus of
    [] -> Map.empty
    x:xs -> Map.insertWith (++) (a,b) [x] (generatePrimitiveModel b x xs)

type IntermediateModel = Map.Map (String,String) [(Integer,String)]

generateIntermediateModel :: PrimitiveModel -> IntermediateModel
generateIntermediateModel = Map.map (reverse . sortBy 
        (comparing fst) . map swap . Map.toList . freqList) where
    freqList a = case a of
        [] -> Map.empty
        x:xs -> Map.insertWith (+) x 1 (freqList xs)

type ProcessedModel = [(String,[(Integer,Integer)])]

processedModel :: IntermediateModel -> ProcessedModel
processedModel model = map snd replacedKeys where
    id_map = generateIDMap 0 (Map.toList model) where
        generateIDMap n [] = Map.empty
        generateIDMap n (x:xs) = Map.insert (fst x) n (generateIDMap (n + 1) xs)

    substitutedReferences = Map.mapWithKey 
        (\(x, y) zs -> map (\(freq, z) -> 
            (freq, Map.findWithDefault 0 (y,z) id_map)) zs) model

    replacedKeys = map (\((x,y), zs) -> 
        (Map.findWithDefault 0 (x,y) id_map, (y, zs))) 
            (Map.toList substitutedReferences)

modelFromHTML :: String -> ProcessedModel
modelFromHTML = processedModel . generateIntermediateModel . 
    generatePrimitiveModel "" "" . corpus . harvest . parseTags

main = do
    content <- readFile "urls.txt"
    html <- bodiesFromURLs (lines content)
    writeFile "sokal.model" $ unlines $ (map show (modelFromHTML html))




