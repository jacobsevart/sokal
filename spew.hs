import Data.Array
import System.Random
import System.IO
import Control.Monad.Trans.State
import System.Environment
import Control.Monad
import Data.List
import Data.Maybe
import qualified Data.Text as Text

type FastModel = Array Int (String, [(Int,Int)])
fastModel :: String -> FastModel
fastModel inp = array (0, length (lines inp)) $ zip [0..] [ read i :: (String, [(Int, Int)]) | i <- (lines inp)]

weightedChoice :: [(Int, Int)] -> Int -> Maybe Int
weightedChoice xs target = fst $ foldl search (Nothing, 0) xs where
    search (Just a, _) _ = (Just a, 0)
    search (Nothing, offset) (weight, next) 
        | weight + offset >= target = (Just next, 0)
        | otherwise = (Nothing, offset + weight)

wordChoice :: FastModel -> State (StdGen, Maybe Int) (Maybe String)
wordChoice model = do
    (gen, maybe_choice) <- get
    case maybe_choice of
        Nothing -> return Nothing
        Just choice -> do            
            let (target, new_gen) = randomR (0, sum $ (map fst) (snd $ model ! choice)) gen
            let next = weightedChoice (snd $ model ! choice) target
            put (new_gen, next)
            return $ fst <$> (!) model <$> next

cleanSpew :: Int -> [String] -> Text.Text
cleanSpew n xs = Text.append (Text.intercalate (Text.pack ". ") $ drop 1 $ (takeNWords n 0 0)) (Text.pack ".") where
    sentences = map Text.strip . Text.splitOn (Text.pack ".") . Text.pack . unwords $ xs :: [Text.Text]
    takeNWords desired taken index
        | taken >= desired = take (index - 1) sentences
        | index == length sentences = take index sentences
        | otherwise = takeNWords desired (sum $ map (length . Text.words) $ take index sentences) (index + 1)

main = do
    args <- getArgs
    model_text <- readFile "sokal.model"
    let model = fastModel model_text
    gen <- getStdGen
    let (first_index, new_gen) = randomR (bounds model) gen
    let spew = evalState (replicateM (read (args !! 0) :: Int) (wordChoice model)) (new_gen, Just first_index)
    print $ cleanSpew (read (args !! 0) :: Int) . catMaybes $ spew
    return 0